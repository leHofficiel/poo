﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Remoting.Metadata.W3cXsd2001;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SpaceInvadersArmory
{
    public class ArmeImporteur
    {
        string fileName=string.Empty;
        int minLength=0;
        Dictionary<string, int> fileInformations = new Dictionary<string, int>();

        public ArmeImporteur(string name, int minLength)
        {
            this.fileName = name;
            this.minLength = minLength;
        }

        public Dictionary<string, int> readFile()
        {
            string text = File.ReadAllText(@"..\..\..\SpaceInvadersArmory\" + fileName);
            text = new string(text.Where(c => !char.IsPunctuation(c)).ToArray());
            string[] textcorr = text.Split(
                new string[] { Environment.NewLine },
                StringSplitOptions.None
            );

            foreach (string line in textcorr)
            {
                if (fileInformations.ContainsKey(line))
                {
                    fileInformations[line]++;
                }
                else
                {
                    fileInformations.Add(line, 1);
                }
            }
            return fileInformations;
        }   
        
        public void addToArmory()
        {

            Random rnd = new Random();

            foreach (KeyValuePair<string, int> info in fileInformations)
            {
                int randomEnum = rnd.Next(0, 3);
                if (info.Value > info.Key.Length)
                {
                    if (isValidWeapon(info.Key))
                    { WeaponBlueprint newWeaponBlueprint = Armory.CreatBlueprint(info.Key, (EWeaponType)randomEnum, info.Key.Length, info.Value); }
                }
                else
                {
                    if (isValidWeapon(info.Key))
                    {
                        WeaponBlueprint newWeaponBlueprint = Armory.CreatBlueprint(info.Key, (EWeaponType)randomEnum, info.Value, info.Key.Length); }
                }
            }
        }

        public bool isValidWeapon(string weap)
        {
            List<string> blackListedWeapon = new List<string>();

            string[] text = File.ReadAllLines(@"..\..\..\SpaceInvadersArmory\blacklist.txt");

            foreach (string line in text)
            {
                blackListedWeapon.Add(line);
            }
            if(blackListedWeapon.Contains(weap) || weap.Length<minLength)
                return false;
            else
                return true;
        }
    }
}
